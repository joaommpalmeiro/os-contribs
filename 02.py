from pathlib import Path

import pandas as pd
from markdown_strings import header
from pandas.io.clipboards import to_clipboard

DATA = Path("data") / "contribs.csv"
MD_SEP = "\n" * 2


def load_data():
    df = pd.read_csv(
        DATA,
        dtype={"Repo": "string", "URL": "string", "Title": "string", "MR/PR": "string"},
    )

    return df


def link_cols(df):
    # df["Repo"] = "[" + df["Repo"] + "]" + "(" + df["URL"] + ")"
    # df["MR/PR"] = "[" + df["Title"] + "]" + "(" + df["MR/PR"] + ")"

    # or

    cols = {
        "Repo": "[" + df["Repo"] + "]" + "(" + df["URL"] + ")",
        "MR/PR": "[" + df["Title"] + "]" + "(" + df["MR/PR"] + ")",
    }
    df = df.assign(**cols)

    return df


def clean_cols(df):
    df = df.drop(columns=["URL", "Title"])

    return df


if __name__ == "__main__":
    title = header("os-contribs", 1)
    description = "My open source contributions."

    df = load_data().pipe(link_cols).pipe(clean_cols)

    print(
        f"Repos: {df['Repo'].nunique(dropna=False)}",
        f"Contributions: {df.shape[0]}",
        sep="\n",
    )

    md_table = df.to_markdown(index=False, tablefmt="github")

    readme = (title, description, md_table)
    md_readme = MD_SEP.join(readme)

    to_clipboard(md_readme, excel=False)

    print("Copied to clipboard!")
