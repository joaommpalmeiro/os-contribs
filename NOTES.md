# Notes

- https://docs.google.com/spreadsheets/d/1o55fbcdsVEIx-dMJcTUT4H1P_ehb9kaCwZjHuQBiY-0/edit?usp=sharing
- https://github.com/pandas-dev/pandas/blob/v2.2.0/pandas/io/clipboards.py#L132: `to_clipboard()`
- https://github.com/pandas-dev/pandas/blob/v2.2.0/pandas/io/clipboard/__init__.py
- https://github.com/pandas-dev/pandas/blob/v2.2.0/pandas/__init__.py
- https://pandas.pydata.org/docs/user_guide/io.html#clipboard
- https://github.com/pandas-dev/pandas/issues/54466
- https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html
- https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.dtypes.html
- https://pandas.pydata.org/docs/user_guide/basics.html#basics-dtypes
- https://pandas.pydata.org/docs/user_guide/text.html#text
- https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_markdown.html
- https://github.com/astanin/python-tabulate?tab=readme-ov-file#table-format
- https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.pipe.html
- https://practicaldatascience.co.uk/data-science/how-to-use-pandas-pipe-to-create-data-pipelines
- https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.assign.html
- https://stackoverflow.com/a/41759638
- https://github.com/pandas-dev/pandas/issues/57123 + https://github.com/pandas-dev/pandas/issues/54466
- https://github.com/pandas-dev/pandas/issues/54466#issuecomment-1919988166
- hrequests:
  - https://pypi.org/project/hrequests/
  - https://daijro.gitbook.io/hrequests
  - https://github.com/daijro/hrequests
  - https://daijro.gitbook.io/hrequests/concurrent-and-lazy-requests/nohup-requests
  - https://daijro.gitbook.io/hrequests/concurrent-and-lazy-requests/easy-concurrency
  - https://daijro.gitbook.io/hrequests/concurrent-and-lazy-requests/grequests-style-concurrency
  - https://daijro.gitbook.io/hrequests/simple-usage/attributes#parse-the-response-body-as-json
  - https://daijro.gitbook.io/hrequests/concurrent-and-lazy-requests/grequests-style-concurrency#imap
  - https://github.com/daijro/hrequests/blob/v0.8.1/pyproject.toml#L29-L50
  - https://github.com/spyoungtech/grequests:
    - "You can also adjust the `size` argument to `map` or `imap` to increase the gevent pool size."
- https://www.gevent.org/
- https://github.com/daijro/browserforge
- https://docs.github.com/en/rest/about-the-rest-api/api-versions?apiVersion=2022-11-28#specifying-an-api-version
- https://docs.github.com/en/rest/pulls/pulls?apiVersion=2022-11-28#get-a-pull-request
- https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
- Pydantic:
  - https://docs.pydantic.dev/latest/concepts/models/#extra-fields
  - https://docs.pydantic.dev/latest/api/networks/#pydantic.networks.HttpUrl
  - https://pydantic.dev/articles/pydantic-v2#more-powerful-aliases
  - https://github.com/koxudaxi/datamodel-code-generator/
- https://github.com/samuelcolvin/python-devtools
- https://github.com/vcs-python/libvcs
- https://libvcs.git-pull.com/
- https://github.com/nephila/giturlparse
- https://github.com/ada-url/ada-python
- https://pypi.org/project/ada-url/
- https://github.com/tktech/can_ada
- https://www.ada-url.com/playground
- https://werkzeug.palletsprojects.com/en/2.3.x/urls/
- https://werkzeug.palletsprojects.com/en/3.0.x/urls/
- https://github.com/litestar-org/fast-query-parsers
- https://docs.python.org/3/library/pathlib.html#pathlib.PurePath.relative_to
- https://github.com/jonschlinkert/parse-github-url
- https://github.com/r1chardj0n3s/parse
- https://docs.github.com/en/rest/using-the-rest-api/rate-limits-for-the-rest-api?apiVersion=2022-11-28#primary-rate-limit-for-unauthenticated-users: "The primary rate limit for unauthenticated requests is 60 requests per hour."
- https://docs.github.com/en/rest/rate-limit/rate-limit?apiVersion=2022-11-28
- https://github.com/dotenvx/dotenvx
- https://pipenv.pypa.io/en/stable/shell.html#automatic-loading-of-env: "Automatic Loading of .env"
- https://docs.gitlab.com/ee/api/rest/index.html#namespaced-path-encoding
- https://stackoverflow.com/questions/946170/equivalent-javascript-functions-for-pythons-urllib-parse-quote-and-urllib-par
- https://adamj.eu/tech/2022/10/14/python-type-hints-exhuastiveness-checking/
- https://github.com/SethMMorton/natsort/wiki/Examples-and-Recipes#sorting-a-pandas-dataframe
- https://natsort.readthedocs.io/en/stable/api.html#natsort.natsort_keygen
- https://github.com/SethMMorton/natsort/wiki/Examples-and-Recipes#controlling-case-when-sorting
- Old URLs:
  - https://github.com/sdispater/tomlkit/pull/282

## Development

Generate the `.env` file and set the environment variables:

```bash
cp .env.example .env
```

```bash
pyenv install && pyenv versions
```

```bash
pip install pipenv==2023.11.15 && pipenv --version
```

```bash
pipenv install --dev --skip-lock
```

```bash
pipenv run codespell
```

```bash
pipenv run ruff check --verbose .
```

```bash
pipenv run ruff check --fix --verbose . && pipenv run ruff format --verbose .
```

```bash
pipenv run python 01.py
```

```bash
pipenv run python 02.py
```

Paste the output of the previous script into the [README.md](README.md) file.

## Commands

```bash
pipenv install --skip-lock \
ada-url==1.13.0 \
gaveta==0.3.0 \
hrequests==0.8.2 \
markdown-strings==3.3.0 \
pandas==2.2.0 \
pydantic==2.8.2 \
tabulate==0.9.0 \
&& pipenv install --dev --skip-lock \
codespell==2.2.6 \
ruff==0.1.11
```

```bash
pipenv --rm
```

```bash
pipenv run codespell --help
```

```bash
curl -L https://api.github.com/repos/jupyterlab/jupyterlab/pulls/16600 | jq
```

```bash
curl -L https://api.github.com/rate_limit
```

```bash
curl -L https://gitlab.com/api/v4/projects/crown-and-flint%2Fopen-film-database/merge_requests/1 | jq
```

```bash
curl -L https://gitlab.com/api/v4/projects/crown-and-flint%2Fopen-film-database | jq
```

## Snippets

```python
with CONTRIBS_OUTPUT.open(mode="w") as f:
    writer = csv.DictWriter(f, fieldnames=["Repo", "URL", "Title", "MR/PR"])
    writer.writeheader()

    for resp in hrequests.imap(reqs, size=len(reqs)):
        data = resp.json()
        pr = PullRequest(**data)

        writer.writerow(
            {
                "Repo": pr.base.repo.full_name,
                "URL": pr.base.repo.html_url,
                "Title": pr.title,
                "MR/PR": pr.html_url,
            }
        )
```
