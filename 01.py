import os
from pathlib import Path
from typing import TypedDict
from urllib.parse import quote_plus

import hrequests
import pandas as pd
from ada_url import URL, join_url
from gaveta.json import read_json
from pydantic import BaseModel, ConfigDict, HttpUrl

BASE_GH_API_URL = "https://api.github.com"
BASE_GL_API_URL = "https://gitlab.com/api/v4"

CONTRIBS_INPUT = Path("data") / "urls.json"
CONTRIBS_OUTPUT = Path("data") / "contribs.csv"


class Metadatum(TypedDict):
    repo: str
    repo_url: HttpUrl


class Repo(BaseModel):
    full_name: str
    html_url: HttpUrl

    model_config = ConfigDict(extra="ignore")


class Base(BaseModel):
    repo: Repo

    model_config = ConfigDict(extra="ignore")


class PullRequest(BaseModel):
    html_url: HttpUrl
    title: str
    base: Base

    model_config = ConfigDict(extra="ignore")


class MergeRequest(BaseModel):
    project_id: int
    title: str
    web_url: HttpUrl

    model_config = ConfigDict(extra="ignore")


class Project(BaseModel):
    id: int
    path_with_namespace: str
    web_url: HttpUrl

    model_config = ConfigDict(extra="ignore")


def gen_gl_api_urls(mr_url: URL) -> tuple[str, str]:
    parsed_pathname = Path(mr_url.pathname)
    pathname_parts = parsed_pathname.relative_to(parsed_pathname.root).parts

    # From: https://gitlab.com/:id/-/merge_requests/:merge_request_iid
    # To: https://gitlab.com/api/v4/projects/:id/merge_requests/:merge_request_iid
    repo_id = quote_plus(f"{pathname_parts[0]}/{pathname_parts[1]}")

    return os.path.join(BASE_GL_API_URL, f"projects/{repo_id}"), os.path.join(
        BASE_GL_API_URL, f"projects/{repo_id}/merge_requests/{pathname_parts[4]}"
    )


def gen_gh_api_url(pr_url: URL) -> str:
    parsed_pathname = Path(pr_url.pathname)
    pathname_parts = parsed_pathname.relative_to(parsed_pathname.root).parts

    # From: https://github.com/OWNER/REPO/pull/PULL_NUMBER
    # To: https://api.github.com/repos/OWNER/REPO/pulls/PULL_NUMBER
    return join_url(
        BASE_GH_API_URL,
        f"repos/{pathname_parts[0]}/{pathname_parts[1]}/pulls/{pathname_parts[3]}",
    )


def sort_rows(df):
    df = df.sort_values(by=["Repo", "Title"], key=lambda col: col.str.casefold())

    return df


if __name__ == "__main__":
    contrib_urls = read_json(CONTRIBS_INPUT)

    gl_metadata_reqs: set[str] = set()
    gl_metadata: dict[int, Metadatum] = {}

    reqs = []
    rows = []

    for contrib_url in contrib_urls:
        parsed_url = URL(contrib_url)

        match parsed_url.hostname:
            case "github.com":
                req = gen_gh_api_url(parsed_url)
                reqs.append(
                    hrequests.async_get(
                        req,
                        headers={
                            "Authorization": f"Bearer {os.environ['GH_PAT']}",
                            "X-GitHub-Api-Version": "2022-11-28",
                        },
                    )
                )
            case "gitlab.com":
                metadata_req, req = gen_gl_api_urls(parsed_url)
                gl_metadata_reqs.add(metadata_req)
                reqs.append(hrequests.async_get(req))
            case _:
                raise AssertionError(
                    f"Unsupported hostname: {parsed_url.hostname}",
                )

    for resp in hrequests.imap(
        [hrequests.async_get(req) for req in gl_metadata_reqs],
        size=len(gl_metadata_reqs),
    ):
        data = resp.json()

        project = Project(**data)

        gl_metadata[project.id] = {
            "repo": project.path_with_namespace,
            "repo_url": project.web_url,
        }

    for resp in hrequests.imap(reqs, size=len(reqs)):
        data = resp.json()

        if resp.url.startswith(BASE_GH_API_URL):
            pr = PullRequest(**data)

            if pr.html_url.unicode_string() not in contrib_urls:
                print(
                    f"{pr.html_url} is not in the list of contribution URLs. The repo may have been moved."
                )

            rows.append(
                {
                    "Repo": pr.base.repo.full_name,
                    "URL": pr.base.repo.html_url,
                    "Title": pr.title,
                    "MR/PR": pr.html_url,
                }
            )
        else:
            mr = MergeRequest(**data)

            if mr.web_url.unicode_string() not in contrib_urls:
                print(
                    f"{mr.web_url} is not in the list of contribution URLs. The repo may have been moved."
                )

            rows.append(
                {
                    "Repo": gl_metadata[mr.project_id]["repo"],
                    "URL": gl_metadata[mr.project_id]["repo_url"],
                    "Title": mr.title,
                    "MR/PR": mr.web_url,
                }
            )

    df = pd.DataFrame.from_records(rows).pipe(sort_rows)
    df.to_csv(CONTRIBS_OUTPUT, index=False)
